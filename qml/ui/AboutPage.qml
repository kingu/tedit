/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This app is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Lomiri.Components 1.3

Page {
    id: aboutPage
    anchors.fill: parent
    visible: false

    header: PageHeader {
        id: header
        title: i18n.tr('About')
    }

    Sections {
        id: header_sections
        width: parent.width  // needed, otherwise the sections are not horizontally swipeable
        anchors {
            top: header.bottom
            topMargin: units.gu(1)
            horizontalCenter: parent.horizontalCenter
        }
        model: [i18n.tr("General"), i18n.tr("Important note"), i18n.tr("Credits")]
    }

    Flickable {
        id: page_flickable

        flickableDirection: Flickable.AutoFlickIfNeeded

        anchors {
            top: header_sections.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }
        contentHeight:  main_column.height + units.gu(2)
        clip: true

        Column {
            id: main_column

            anchors {
                left: parent.left
                right: parent.right
                top: parent.top
                leftMargin: units.gu(2)
                rightMargin: units.gu(2)
            }

            /*** Start 'General' section tab ***/
            Item {
                id: icon

                visible: header_sections.selectedIndex === 0
                width: parent.width
                height: app_icon.height + units.gu(4)

                LomiriShape {
                    id: app_icon

                    width: Math.min(aboutPage.width/3, 256)
                    height: width
                    anchors.centerIn: parent

                    source: Image {
                        id: icon_image
                        source: Qt.resolvedUrl("../../assets/tedit.png")
                    }
                    radius: "small"
                    aspect: LomiriShape.DropShadow
                }
            }

            Label {
                id: name

                visible: header_sections.selectedIndex === 0
                text: i18n.tr("tedit") + " v%1".arg(Qt.application.version)
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.bottomMargin: units.gu(4)
                textSize: Label.Large
                horizontalAlignment:  Text.AlignHCenter
            }

            Repeater {
                id: aboutLinks

                model: [
                { name: i18n.tr("Source code"), url: "https://gitlab.com/Danfro/tedit" },
                { name: i18n.tr("License") + ": GNU GPLv3", url: "https://www.gnu.org/licenses/quick-guide-gplv3.html" },
                { name: i18n.tr("tedit in OpenStore"), url: "https://open-store.io/app/danfro.tedit" },
                { name: i18n.tr("Report bugs"),  url: "https://gitlab.com/Danfro/tedit/issues" },
                { name: i18n.tr("Translate on Weblate"), url: "https://hosted.weblate.org/projects/ubports/tedit/" },
                { name: i18n.tr("Changelog"), url: "https://gitlab.com/Danfro/tedit/-/blob/main/CHANGELOG.md" }
                ]

                delegate: ListItem {
                    divider { visible: false; }
                    visible: header_sections.selectedIndex === 0
                    height: layoutAbout.height
                    ListItemLayout {
                        id: layoutAbout
                        title.text : modelData.name
                        ProgressionSlot {name: "external-link"; }
                    }
                    onClicked: Qt.openUrlExternally(modelData.url)
                }
            }

            Rectangle {
                // spacer
                width: parent.width
                height: units.gu(2)
                color: "transparent"
            }

            Repeater {
                id: donateLinks

                model: [
                    { name: i18n.tr("Donate to UBports"), subtitle: i18n.tr("UBports supports the development of Ubuntu Touch."), url: "https://ubports.com/en_EN/donate" },
                    { name: i18n.tr("Donate to me"), subtitle: i18n.tr("I am maintaining and improving this app."),url: "https://paypal.me/payDanfro" }
                ]

                delegate: ListItem {
                    divider { visible: false; }
                    visible: header_sections.selectedIndex === 0
                    height: layoutAbout.height
                    ListItemLayout {
                        id: layoutAbout
                        title.text : modelData.name
                        subtitle.text: modelData.subtitle
                        ProgressionSlot {name: "external-link"; }
                    }
                    onClicked: Qt.openUrlExternally(modelData.url)
                }
            }
            /*** End 'General' section tab ***/

            /*** Start 'Important note' section tab ***/
            Label {
                id: noteHeader

                visible: header_sections.selectedIndex === 1
                text: i18n.tr("IMPORTANT note on encrypted files:")
                anchors.horizontalCenter: parent.horizontalCenter
                font.bold : true
                horizontalAlignment:  Text.AlignHCenter
            }
            Label {
                id: noRestoreNote

                visible: header_sections.selectedIndex === 1
                font.bold : true
                horizontalAlignment: Text.AlignJustify
                width: parent.width
                text: "\n"
                    +i18n.tr("If the encryption key is lost, there is NO restore. The file remains encrypted! The app author takes no responsibility!")
                    + "\n"
                wrapMode: Text.WordWrap
            }
            Label {
                id: encrytionNote

                visible: header_sections.selectedIndex === 1
                horizontalAlignment: Text.AlignJustify
                width: parent.width
                text: i18n.tr("Encrypted file names have a \"XXX\" suffix. A padlock is shown, when an encrypted file is currently open.")
                wrapMode: Text.WordWrap
            }
            Label {
                id: storageLocationNote

                visible: header_sections.selectedIndex === 1
                horizontalAlignment: Text.AlignJustify
                width: parent.width
                text: "\n" + i18n.tr("Files are stored in the following folder on your device:") 
                wrapMode: Text.WordWrap
            }
            Label {
                id: storageNote

                visible: header_sections.selectedIndex === 1
                horizontalAlignment: Text.AlignJustify
                width: parent.width
                text: root.fileSavingPath
                color: theme.palette.normal.activity 
                wrapMode: Text.WordWrap
            }
            /*** End 'Important note' section tab ***/

            /*** Start 'Credits' section tab ***/
            Label {
                id: actualdev

                visible: header_sections.selectedIndex === 2
                text: "\n" + i18n.tr("App development since version v3.0.0")
                anchors.horizontalCenter: parent.horizontalCenter
                font.bold : true
                horizontalAlignment:  Text.AlignHCenter
            }

            ListItem {
                visible: header_sections.selectedIndex === 2
                divider { visible: false; }
                height: l_maintainer.height + divider.height
                ListItemLayout {
                    id: l_maintainer
                    title.text: i18n.tr("Maintainer") + ": Daniel Frost"
                    ProgressionSlot {name: "external-link"; }
                }
                onClicked: {Qt.openUrlExternally('https://gitlab.com/Danfro')}
            }

            ListItem {
                visible: header_sections.selectedIndex === 2
                divider { visible: false; }
                height: gitlabGraphs.height + divider.height
                ListItemLayout {
                    id: gitlabGraphs
                    title.text: i18n.tr("Contributors") + ": are listed on Gitlab"
                    ProgressionSlot {name: "external-link"; }
                }
                onClicked: {Qt.openUrlExternally('https://gitlab.com/Danfro/tedit/-/graphs/main')}
            }

            Rectangle {
                // spacer
                width: parent.width
                height: units.gu(2)
                color: "transparent"
            }

            Label {
                id: historicaldev

                visible: header_sections.selectedIndex === 2
                text: "\n" + i18n.tr("App development up to version v2.8.4")
                anchors.horizontalCenter: parent.horizontalCenter
                font.bold : true
                horizontalAlignment:  Text.AlignHCenter
            }

            Repeater {
                id: historicalDevCredits

                model: [
                    { name: i18n.tr("Author") + ": fulvio", url: "https://github.com/fulvio999" },
                    { name: i18n.tr("Author") + ": Paweł Stroka" }, 
                    { name: i18n.tr("Code") + "  : " + i18n.tr("jshashes library for hash Algorithm"), url: "https://github.com/h2non/jshashes" },
                    { name: i18n.tr("Code") + "  : " + i18n.tr("Base64 features"), url: "https://github.com/mathiasbynens/base64" },
                    { name: i18n.tr("Code") + "  : " + i18n.tr("M4rtinK, QML binding for qr.js lib"), url: "https://github.com/M4rtinK" },
                    { name: i18n.tr("Code") + "  : " + i18n.tr("Developers of CriptoJS library"), url: "https://cryptojs.gitbook.io/docs/" }
                ]

                delegate: ListItem {
                    divider { visible: false; }
                    visible: header_sections.selectedIndex === 2
                    height: layoutAbout.height
                    ListItemLayout {
                        id: layoutAbout
                        title.text : modelData.name
                        ProgressionSlot {name: "external-link"; visible: modelData.url ? true : false }
                    }
                    onClicked: Qt.openUrlExternally(modelData.url)
                }
            }

            ListItem {
                visible: header_sections.selectedIndex === 2
                divider { visible: false; }
                height: githubGraphs.height + divider.height
                ListItemLayout {
                    id: githubGraphs
                    title.text: i18n.tr("Contributors") + ": are listed on Github"
                    ProgressionSlot {name: "external-link"; }
                }
                onClicked: {Qt.openUrlExternally('https://github.com/fulvio999/tedit/graphs/contributors')}
            }
            /*** End 'Credits' section tab ***/
        }
    }
}
