/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This app is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Lomiri.Components 1.3
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0

import "../components"
import "../js/qqr.js" as QRCodeBackend

/*
Page where is shown the QR Code associated at the chosen url
*/
Page {
    id: qrCodeGeneratorPage
    /* the url to represent with a QR Code */
    property string pageUrl;

    anchors.fill: parent
    visible: false

    header: PageHeader {
        id: header
        title: i18n.tr("Page")+": "+pageUrl
    }

    QRCode {
        id: qrcodeComponent
        anchors.top : header.bottom
        anchors.topMargin : units.gu(4)
        anchors.leftMargin : units.gu(4)
        anchors.horizontalCenter : parent.horizontalCenter
        anchors.verticalCenter : parent.verticalCenter

        width : parent.width - units.gu(3)
        height : parent.height - units.gu(3)
        value : pageUrl
        level : "H"
    }
}
