/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This app is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3


/*
   General info about the selected saved file
 */
Dialog {
    id: fileInfoDialog
    contentWidth: units.gu(45)
    title: i18n.tr("File Info")

    Label{
        text: i18n.tr("File Size")+": " +  localFilePickerPage.selectedFileSize +" bytes"
    }

    Label{
        text: i18n.tr("Modified")+": "+localFilePickerPage.selectedFileModificationDate
    }
    Label{
        text: "(" + i18n.tr("Time format (UTC): 'yyyy-mm-dd-HH:mm:ss'") + ")"
    }

    Button {
        text:  i18n.tr("Close")
        onClicked: PopupUtils.close(fileInfoDialog)
    }
}
