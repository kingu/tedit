/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This app is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Lomiri.Components 1.3
import Qt.labs.settings 1.0

/*
Application settings pages about font and colours
*/
Page {
    id: settingsPage

    header: PageHeader {
        title: i18n.tr("Settings")
    }

    visible: false

    function checkBW(checkColor) {
        if (Qt.colorEqual(settings.textAreaFontColor,checkColor))  {
            if (Qt.colorEqual(settings.textAreaFontColor,"#ffffff")) {
                settings.textAreaFontColor = "black"
                textArea.color = "black"
            } else if (Qt.colorEqual(settings.textAreaFontColor,"#000000")) {
                settings.textAreaFontColor = "white"
                textArea.color = "white"
            }
        }
    }

    Flickable {
        id: settingsPageFlickable
        clip: true
        flickableDirection: Flickable.AutoFlickIfNeeded
        boundsBehavior: Flickable.StopAtBounds
        contentHeight: settingsColumn.childrenRect.height
        anchors {
                top: parent.top
                left: parent.left
                right: parent.right
                bottom: settingsPage.bottom
                bottomMargin: units.gu(1)
        }

        Column {
            id: settingsColumn
            anchors.fill: parent
            visible: parent.visible

            /* placeholder */
            Rectangle {
                color: "transparent"
                width: parent.width
                height: units.gu(5)
            }

            ListItem {

                Label {
                    id:enableWordWrapLabel
                    text: i18n.tr("Enable Word wrap")
                    anchors {
                        leftMargin: units.gu(2)
                        left: parent.left
                        verticalCenter: parent.verticalCenter
                    }
                }

                Switch {
                    id: wrap
                    checked: settings.wordWrap
                    text: i18n.tr("Word wrap")
                    onCheckedChanged: {
                        settings.wordWrap = checked
                        if(checked)
                            textArea.wrapMode = TextEdit.Wrap
                        else
                            textArea.wrapMode = TextEdit.NoWrap
                    }

                    anchors {
                        rightMargin: units.gu(2)
                        right: parent.right
                        verticalCenter: parent.verticalCenter
                    }
                }
                onClicked: wrap.checked = !wrap.checked
            }

            ListItem {
                Label {
                    id:enablePredictionLabel
                    text: i18n.tr("Enable text prediction")
                    anchors {
                        leftMargin: units.gu(2)
                        left: parent.left
                        verticalCenter: parent.verticalCenter
                    }
                }

                Switch {
                    id: pred
                    checked: settings.textPrediction
                    text: i18n.tr("Text prediction")
                    onCheckedChanged: {
                        settings.textPrediction = checked
                        if(checked)
                            textArea.inputMethodHints = Qt.ImhMultiLine
                        else
                            textArea.inputMethodHints = Qt.ImhMultiLine | Qt.ImhNoPredictiveText
                    }

                    anchors {
                            rightMargin: units.gu(2)
                            right: parent.right
                            verticalCenter: parent.verticalCenter
                    }

                }
                onClicked: pred.checked = !pred.checked
            }

            ListItem {
                Label {
                    text: i18n.tr("Notes Area Font color")
                    font.bold: true
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.topMargin: units.gu(1)
                }
            }

            ColorListItem {
                id: greenFont
                label: i18n.tr("Green")
                itemColor: theme.palette.normal.positive
                onClicked: {
                    settings.textAreaFontColor = itemColor
                    textArea.color = itemColor
                    pageStack.pop()
                }
            }

            ColorListItem {
                id: yellowFont
                visible: !Qt.colorEqual(settings.pageBackgroundColor,"#ffffff")
                label: i18n.tr("Yellow")
                itemColor: "yellow"
                onClicked: {
                    settings.textAreaFontColor = itemColor
                    textArea.color = itemColor
                    pageStack.pop()
                }
            }

            ColorListItem {
                id: greyFont
                label: i18n.tr("Grey")
                itemColor: theme.palette.normal.backgroundTertiaryText
                onClicked: {
                    settings.textAreaFontColor = itemColor
                    textArea.color = itemColor
                    pageStack.pop()
                }
            }

            ColorListItem {
                id: blackFont
                label: i18n.tr("Black")
                visible: Qt.colorEqual(settings.pageBackgroundColor,"#ffffff")
                itemColor: "black"
                onClicked: {
                    settings.textAreaFontColor = itemColor
                    textArea.color = itemColor
                    pageStack.pop()
                }
            }

            /* ------------ Application background colors -------------- */

            ListItem {
                Label {
                    text: i18n.tr("Application Backgroung color")
                    font.bold: true
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.topMargin: units.gu(1)
                }
            }

            ColorListItem {
                id: lightColorChooser
                label: i18n.tr("Light blue")
                itemColor: '#126381'
                onClicked: {
                    settings.pageBackgroundColor = itemColor
                    root.backgroundColor = itemColor
                    checkBW("#000000")
                    pageStack.pop()
                }
            }

            ColorListItem {
                id: brownColorChooser
                label: i18n.tr("Brown")
                itemColor: '#8F4B0E'
                onClicked: {
                    settings.pageBackgroundColor = itemColor
                    root.backgroundColor = itemColor
                    checkBW("#000000")
                    pageStack.pop()
                }
            }

            ColorListItem {
                id: whiteColorChooser
                label: i18n.tr("White")  /* Default */
                itemColor: "white"
                onClicked: {
                    settings.pageBackgroundColor = itemColor
                    root.backgroundColor = itemColor
                    checkBW("#ffffff")
                    pageStack.pop()
                }
            }

            ColorListItem {
                id: darkBlueColorChooser
                label: i18n.tr("Dark blue")
                itemColor: '#1E2F3F'
                onClicked: {
                    settings.pageBackgroundColor = itemColor
                    root.backgroundColor = itemColor
                    checkBW("#000000")
                    pageStack.pop()
                }
            }

            ListItem {
                id:fontContainer
                divider.visible : false
                height: units.gu(12)
                Label {
                    id:fontLabel
                    text: i18n.tr("Notes font size")+": "+textArea.font.pixelSize
                    anchors {
                        leftMargin: units.gu(2)
                        left: parent.left
                        verticalCenter: parent.verticalCenter
                    }
                }

                Slider {
                    id: slider
                    function formatValue(v) { return v.toFixed(1) }
                    minimumValue: 20
                    maximumValue: 80
                    value: settings.pixelSize
                    live: false
                    onValueChanged: {
                        textArea.font.pixelSize = formatValue(value) * 1
                        settings.pixelSize = formatValue(value) * 1
                    }

                    anchors {
                        leftMargin: units.gu(3)
                        left: fontLabel.right
                        rightMargin: units.gu(2)
                        right: parent.right
                        verticalCenter: parent.verticalCenter
                    }
                }
            }
        } //col

    } //Flickable

    Scrollbar {
        flickableItem: settingsPageFlickable
        align: Qt.AlignTrailing
    }

}
