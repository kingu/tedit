/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This app is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Lomiri.Components 1.3
import EdIt 1.0
import Lomiri.Content 1.1
import Lomiri.Components.Popups 1.3
import Qt.labs.settings 1.0
import Qt.labs.platform 1.0 as PF //for StandardPaths
import "components"
import "ui"

/* digest calculator functions */
import "js/hashes.js" as Hashes
import "js/base64.js" as Base64enc

/* Crypto lib for file protection */
import "js/crypto-js/crypto-js.js" as CryptoJSLib

import "js/Utility.js" as Utility


MainView {

    id: root
    objectName: "mainView"

    /* Note! applicationName needs to match the "name" field of the click manifest */
    applicationName: "tedit.danfro"
    property string appVersion : "v%1".arg(Qt.application.version)

    /* application hidden folder where are saved the files. (path is fixed due to Appp confinement rules) */
    property string fileSavingPath: PF.StandardPaths.writableLocation(PF.StandardPaths.AppDataLocation).toString().replace("file://","") + "/"

    automaticOrientation: true
    anchorToKeyboard: true

    /* enable to test with dark theme */
    theme.name: ""

    /*------- Tablet (width >= 110) -------- */
    //vertical
    //width: units.gu(75)
    //height: units.gu(111)

    //horizontal (for release)
    width: units.gu(100)
    height: units.gu(75)

    /* ----- phone 4.5 (the smallest one) ---- */
    //vertical
    //width: units.gu(50)
    //height: units.gu(72)

    //horizontal
    //width: units.gu(96)
    //height: units.gu(50)
    /* -------------------------------------- */

    /* the text to show in the operation result popUp */
    property string infoText: ""

    Settings {
        id: settings
        /* by default disable autocomplete in text area */
        property bool textPrediction: false
        property bool wordWrap: false
        property string showAlert: "true"
        property string pageBackgroundColor: theme.palette.normal.activityText
        property string textAreaFontColor: LomiriColors.jet
        property int pixelSize: 40 /* default */
    }

    Component.onCompleted: {
        root.backgroundColor = settings.pageBackgroundColor

        if(settings.showAlert == "true"){
            showInfo(i18n.tr("If the encryption key is lost, there is NO restore. The file remains encrypted! The app author takes no responsibility!") + i18n.tr("Encrypted file names have a \"XXX\" suffix. A padlock is shown, when an encrypted file is currently open."));
            settings.showAlert = "false"
        }
    }

    /* To notify messages at the user */
    Component {
        id: popover
        Dialog {
            id: po
            text: "<b>"+infoText+"</b>"
            MouseArea {
                anchors.fill: parent
                onClicked: PopupUtils.close(po)
            }
        }
    }

    /* Menu Chooser */
    Component {
        id: menuPopover
        Dialog {
            id: menuDialog
            text: "<b>"+infoText+"</b>"
            MouseArea {
                anchors.fill: parent
                onClicked: PopupUtils.close(menuDialog)
            }
        }
    }

    Component {
        id: confirmClearAll
        ConfirmClearAll{}
    }

    Component {
        id: confirmPasteFromClipboard
        ConfirmPasteFromClipboard{}
    }

    /* custom C++ plugin to save/manage files on the filesystem */
    FileIO {
        id: fileIO
    }

    /*
        Encrypt the provided content using the provided key
    */
    function encrypt(content, key) {
        var iv= '16BytesLengthKey';
        var CryptoJS = CryptoJSLib.CryptoJS;
        var AES = CryptoJS.AES;
        var ivStr  = CryptoJS.enc.Utf8.parse(iv);
        var keyStr = CryptoJS.enc.Utf8.parse(key);

        var text = AES.encrypt(content, keyStr, {
                                    iv: ivStr,
                                    mode: CryptoJS.mode.OFB,
                                    padding: CryptoJS.pad.Iso10126
                                });
        return text.toString();
    }

    /*
        DEcrypt the provided ciphertext using the provided key
    */
    function decrypt(ciphertext, key) {
            var iv= '16BytesLengthKey';
            var CryptoJS = CryptoJSLib.CryptoJS;
            var AES = CryptoJS.AES;
            var ivStr  = CryptoJS.enc.Utf8.parse(iv);
            var keyStr = CryptoJS.enc.Utf8.parse(key);

            /* Note: decrypt works only with this values of 'mode' and 'padding' */
            var bytes = AES.decrypt(ciphertext, keyStr, {
                                        iv: ivStr,
                                        mode: CryptoJS.mode.OFB,
                                        padding: CryptoJS.pad.Iso10126
                                    });

            var plaintext = bytes.toString(CryptoJS.enc.Utf8);

            return plaintext.toString();
    }

    /*
        Used when the user press "Save" button in the menu for an already saved file
        whose content is just updated
    */
        function saveExistingFile(filename, destinationDir) {

            /* console.log("Saving existing file..."); */

            /* path and fileName */
            var destinationFolder = "file://" + destinationDir + fileIO.getFullName(filename);

            /* dummy solution to prevent content append */
            if(fileIO.exists(destinationFolder)){
                fileIO.remove(destinationFolder)
            }

            var contentToSave;
            /* current file could be previously saved as Encrypted or not... */
            if(mainPage.fileEncrypted === true){
                contentToSave = encrypt(textArea.text, mainPage.encryptionPassword);
            }else{
                contentToSave = textArea.text;
            }

            fileIO.write(destinationFolder, contentToSave);
            showInfo(i18n.tr("Saved"));

            /* to prevent opening UnSavedDialog */
            mainPage.saved = true;
            currentFileOpenedLabel.color = theme.palette.normal.positive
        }

    /* show a Popup containing the provided in argument input */
    function showInfo(info) {
        infoText = "\n" + info + "\n";
        PopupUtils.open(popover);
    }

    SettingsPage {
        id: settingsPage
    }

    AboutPage {
        id: aboutPage
    }

    UnsavedDialog  {
        id: unsavedDialog;
        property Action closeAction
    }

    /* PopUp with the menu */
    MenuOptions {
        id: menuPage
    }

    /* Ask for a web-site url to import as text in the textArea */
    Component {
        id: webSiteSelector
        WebSiteSelector{}
    }


    /* Ask for a web-site url to generate the associated QR Code */
    Component {
        id: qrCodeWebSiteSelector
        QrCodeWebSiteSelector{}
    }

    Component {
        id: saveAsDialog
        SaveAsDialog{}
    }

    Base64conversionPage {
        id: base64conversionPage
    }

    QrCodeGeneratorPage {
        id: qrCodeGeneratorPage
    }


    /* renderer for the entry in the Digest OptionSelector */
    Component {
        id: digestChooserDelegate
        OptionSelectorDelegate { text: name; }
    }

    Component {
            id: digestCalculatorChooser
            DigestCalculator{inputText: textArea.text}
    }


    PageStack {
        id: pageStack

        Component.onCompleted: {
            pageStack.push( mainPage )
        }

        /* Application main page */
        Page {

            id: mainPage
            anchors.fill: parent

            property Item textArea
            property bool saved: true /* a flag: true if file is NOT currently modified */
            property string openedFileName: ""  /* the currently opened file name */
            property bool currentFileLabelVisible: false /* to hide/show label with the current file name */
            property bool fileEncrypted: false /* set to "Encrypted" if the file is saved as encrypted */
            property string encryptionPassword: "" /* the encryptionkey of the currently opened file */

            header: PageHeader {

                id: header

                ClickableHeaderIcon {
                        id: menu_button
                        iconName: "navigation-menu"
                        text: i18n.tr("Menu")
                        anchors {
                            left: parent.left
                            leftMargin: units.gu(2)
                            verticalCenter: header.verticalCenter
                        }
                        onTriggered: {
                            pageStack.push(menuPage)
                            menuPage.visible = true
                        }
                }

                Label {
                    id: titleLabel
                    text: i18n.tr("tedit")
                    textSize: Label.Large
                    anchors {
                        horizontalCenter: header.horizontalCenter
                        verticalCenter: header.verticalCenter
                    }
                }

                ClickableHeaderIcon {
                        id: settings_button
                        iconname: "settings"
                        text: i18n.tr("Settings")
                        anchors {
                            right: about_button.left
                            rightMargin: units.gu(2)
                            verticalCenter: header.verticalCenter
                        }
                        onTriggered: {
                            pageStack.push(settingsPage)
                            settingsPage.visible = true
                        }
                }

                ClickableHeaderIcon {
                        id: about_button
                        iconname: "help"
                        text: i18n.tr("About")
                        anchors {
                                right: header.right
                                rightMargin: units.gu(2)
                                verticalCenter: header.verticalCenter
                            }
                            onTriggered: {
                                pageStack.push(aboutPage)
                                aboutPage.visible = true
                            }
                    }
            }

            Flickable {
                id: resultPageFlickable
                clip: true
                flickableDirection: Flickable.AutoFlickIfNeeded
                boundsBehavior: Flickable.StopAtBounds
                contentHeight: mainPageLayout.childrenRect.height
                anchors {
                        top: parent.top
                        left: parent.left
                        right: parent.right
                        bottom: mainPage.bottom
                        bottomMargin: units.gu(1)
                }

                Column {
                    id: mainPageLayout
                    anchors.fill: parent
                    spacing: units.gu(1)

                    anchors {
                        margins: units.gu(1)
                        topMargin: units.gu(7)
                    }

                    /* show the name of the currently opened file name */
                    Rectangle{
                            id: currentFileOpenedContainer
                            color: root.backgroundColor
                            width: parent.width
                            height: units.gu(3)

                            Label{
                                id: currentFileOpenedLabel
                                visible: mainPage.currentFileLabelVisible
                                anchors.verticalCenter: parent.verticalCenter
                                text: mainPage.openedFileName+"     ("+i18n.tr("Saved")+": "+ mainPage.saved+")"
                                font.bold: true
                                /* happen when a new file is opened */
                                onTextChanged: {
                                    currentFileOpenedLabel.color = theme.palette.normal.positive
                                }
                            }

                            Image {
                                id: padlockIcon
                                visible: mainPage.fileEncrypted
                                source: Qt.resolvedUrl("../assets/encrypted.png")
                                height: units.gu(3)
                                width: units.gu(3)

                                anchors { 
                                    left: currentFileOpenedLabel.right
                                    rightMargin: units.gu(2)
                                    verticalCenter: parent.verticalCenter
                                }
                            }
                    }

                    Rectangle {
                        id: textAreaContainer
                        width: parent.width
                        height: root.height - currentFileOpenedContainer.height - units.gu(12)
                        radius: 20
                        border.color : theme.palette.normal.backgroundTertiaryText
                        border.width : units.gu(0.1)

                        /* Display the file content */
                        TextArea {
                            id: textArea
                            anchors.centerIn: parent
                            width: parent.width - (2 * parent.border.width)
                            height: parent.height - (2 * parent.border.width)
                            textFormat: TextEdit.AutoText
                            font.pixelSize: settings.pixelSize
                            placeholderText: i18n.tr("Welcome ! Write what you want and save it. Enjoy!")
                            inputMethodHints: settings.textPrediction ? Qt.ImhMultiLine : Qt.ImhMultiLine | Qt.ImhNoPredictiveText
                            selectByMouse: true
                            onTextChanged: {
                                /* update flag file modified and not saved */
                                mainPage.saved = false;
                                currentFileOpenedLabel.color = '#A40000'
                            }
                            wrapMode: settings.wordWrap ? TextEdit.Wrap : TextEdit.NoWrap

                            Component.onCompleted: {
                                color =  settings.textAreaFontColor
                                textAreaContainer.color = settings.textAreaFontColor
                            }
                        }
                    }

                    Rectangle{
                        id:infoBarContainer
                        color: root.backgroundColor
                        width: parent.width
                        height: units.gu(3)
                        Label{
                            text: i18n.tr("Line count")+": "+textArea.lineCount+ "  "+ i18n.tr("Character count")+": "+textArea.length
                            font.bold: true
                        }
                    }
                } //column
            } //flick
        }
    } //pageStack
}
