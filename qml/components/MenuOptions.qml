/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This app is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Lomiri.Components 1.3
import EdIt 1.0
import Lomiri.Content 1.1
import Lomiri.Components.Popups 1.3

/*
Page that displays the menu options
*/

Page {
      id: menuPage
      visible: false

      header: PageHeader {
            id: header
            title: i18n.tr("Choose an action:")
      }

      Flickable {
            id: settingsPageFlickable2
            clip: true
            flickableDirection: Flickable.AutoFlickIfNeeded
            boundsBehavior: Flickable.StopAtBounds
            contentHeight: mainColumn.childrenRect.height

            anchors {
                  top: header.bottom
                  margins: units.gu(2)
                  left: parent.left
                  right: parent.right
                  bottom: parent.bottom
            }

            Column{
                  id: mainColumn
                  anchors.fill: parent
                  spacing: units.gu(0.5)

                  

                  //---------------------- OPEN -------------
                  ListItem {
                        id:openContainer
                        anchors.horizontalCenter: parent.Center
                        height: units.gu(4.5)
                        divider.visible : true

                        Icon {
                              id: openIcon
                              name: "document-open"
                              height: units.gu(4)
                              width: units.gu(4)

                              anchors {
                                    leftMargin: units.gu(3)
                                    rightMargin: units.gu(2)
                                    verticalCenter: parent.verticalCenter
                              }
                        }

                        MouseArea {
                              id: selectableMouseAreaOpen
                              width: parent.width
                              height: parent.height

                              onClicked: {
                                    pageStack.pop()
                                    pageStack.push(Qt.resolvedUrl("../ui/LocalFilePickerPage.qml"))
                              }
                        }

                        Label {
                              text: i18n.tr("Open")
                              anchors {
                                    left: openIcon.right
                                    leftMargin: units.gu(2)
                                    rightMargin: units.gu(2)
                                    verticalCenter: parent.verticalCenter
                              }
                        }
                  }

                  //------------------------ SAVE -------------------
                  ListItem {
                        id:saveContainer
                        anchors.horizontalCenter: parent.Center
                        height: units.gu(4.5)
                        divider.visible : true

                        Icon {
                              id: saveIcon
                              name: "document-save"
                              height: units.gu(4)
                              width: units.gu(4)

                              anchors {
                                    leftMargin: units.gu(2)
                                    rightMargin: units.gu(2)
                                    verticalCenter: parent.verticalCenter
                              }
                        }

                        MouseArea {
                              id: selectableMouseAreaSave
                              anchors.fill: parent

                              onClicked: {
                                    if(mainPage.openedFileName == "") { /* true if file is new, never saved  */
                                          PopupUtils.open(saveAsDialog)
                                    } else { /* file not new: already exist: just update content */
                                          /* function in Main.qml file */
                                          saveExistingFile(mainPage.openedFileName,root.fileSavingPath)
                                    }
                                    pageStack.pop()
                              }
                        }

                        Label {
                              text: i18n.tr("Save")
                              anchors {
                                    left: saveIcon.right
                                    leftMargin: units.gu(2)
                                    rightMargin: units.gu(2)
                                    verticalCenter: parent.verticalCenter
                              }
                        }
                  }

                  //--------------------- SAVE AS -----------------------
                  ListItem {
                        id:saveAsContainer
                        anchors.horizontalCenter: parent.Center
                        height: units.gu(4.5)
                        divider.visible : true

                        Icon {
                              id: saveAsIcon
                              name: "document-save-as"
                              height: units.gu(4)
                              width: units.gu(4)

                              anchors {
                                    leftMargin: units.gu(2)
                                    rightMargin: units.gu(2)
                                    verticalCenter: parent.verticalCenter
                              }
                        }

                        MouseArea {
                              id: selectableMouseAreaSaveAs
                              width: parent.width
                              height: parent.height

                              onClicked: {
                                    PopupUtils.open(saveAsDialog)
                                    pageStack.pop()
                              }
                              }

                              Label {
                                    text: i18n.tr("Save as")
                                    anchors {
                                          left: saveAsIcon.right
                                          leftMargin: units.gu(2)
                                          rightMargin: units.gu(2)
                                          verticalCenter: parent.verticalCenter
                                    }
                              }
                  }

                  //------------------- Undo --------------------
                  ListItem {
                        id:undoContainer
                        anchors.horizontalCenter: parent.Center
                        height: units.gu(4.5)
                        divider.visible : true

                        Icon {
                              id: undoIcon
                              name: "edit-undo"
                              height: units.gu(4)
                              width: units.gu(4)

                              anchors {
                                    leftMargin: units.gu(2)
                                    rightMargin: units.gu(2)
                                    verticalCenter: parent.verticalCenter
                              }
                        }

                        MouseArea {
                              id: selectableMouseAreaUndo
                              width: parent.width
                              height: parent.height

                              onClicked: {
                                    textArea.undo()
                                    pageStack.pop()
                              }
                        }

                        Label {
                              text: i18n.tr("Undo last modification")
                              anchors {
                                    left: undoIcon.right
                                    leftMargin: units.gu(2)
                                    rightMargin: units.gu(2)
                                    verticalCenter: parent.verticalCenter
                              }
                        }
                  }

                  //----------------------- Redo ---------------
                  ListItem {
                        id:redoContainer
                        anchors.horizontalCenter: parent.Center
                        height: units.gu(4.5)
                        divider.visible : true

                        Icon {
                              id: redoIcon
                              name: "edit-redo"
                              height: units.gu(4)
                              width: units.gu(4)

                              anchors {
                                    leftMargin: units.gu(2)
                                    rightMargin: units.gu(2)
                                    verticalCenter: parent.verticalCenter
                              }
                        }

                        MouseArea {
                              id: selectableMouseAreaRedo
                              width: parent.width
                              height: parent.height

                              onClicked: {
                                    textArea.redo()
                                    pageStack.pop()
                              }
                        }

                        Label {
                              text: i18n.tr("Redo last modification")
                              anchors {
                                    left: redoIcon.right
                                    leftMargin: units.gu(2)
                                    rightMargin: units.gu(2)
                                    verticalCenter: parent.verticalCenter
                              }
                        }
                  }

                  //-------------------------- SELECT ALL ---------------------
                  ListItem {
                        id:selectAllContainer
                        anchors.horizontalCenter: parent.Center
                        height: units.gu(4.5)
                        divider.visible : true

                        Icon {
                              id: selectAllIcon
                              name: "edit"
                              height: units.gu(4)
                              width: units.gu(4)

                              anchors {
                                    leftMargin: units.gu(2)
                                    rightMargin: units.gu(2)
                                    verticalCenter: parent.verticalCenter
                              }
                        }

                        MouseArea {
                              id: selectableMouseAreaSelectAll
                              width: parent.width
                              height: parent.height

                              onClicked: {
                              textArea.selectAll();
                              pageStack.pop()
                              }
                        }

                        Label {
                              text: i18n.tr("Select all")
                              anchors {
                                    left: selectAllIcon.right
                                    leftMargin: units.gu(2)
                                    rightMargin: units.gu(2)
                                    verticalCenter: parent.verticalCenter
                              }
                        }
                  }

                  //--------------- CLEAR ----------------
                  ListItem {
                        id:clearContainer
                        anchors.horizontalCenter: parent.Center
                        height: units.gu(4.5)
                        divider.visible : true

                        Icon {
                              id: clearIcon
                              name: "delete"
                              height: units.gu(4)
                              width: units.gu(4)

                              anchors {
                                    leftMargin: units.gu(2)
                                    rightMargin: units.gu(2)
                                    verticalCenter: parent.verticalCenter
                              }
                        }

                        MouseArea {
                              id: selectableMouseAreaClear
                              width: parent.width
                              height: parent.height

                              onClicked: {
                              PopupUtils.open(confirmClearAll)
                              pageStack.pop()
                              }
                        }

                        Label {
                              text: i18n.tr("Clear area")
                              anchors {
                                    left: clearIcon.right
                                    leftMargin: units.gu(2)
                                    rightMargin: units.gu(2)
                                    verticalCenter: parent.verticalCenter
                              }
                        }
                  }

                  //-------------------- PASTE FROM CLIPBOARD -------------
                  ListItem {
                        id:pasteFromClipboardContainer
                        anchors.horizontalCenter: parent.Center
                        height: units.gu(4.5)
                        divider.visible : true

                        Icon {
                              id: pasteFromClipboardIcon
                              name: "edit-paste"
                              height: units.gu(4)
                              width: units.gu(4)

                              anchors {
                                    leftMargin: units.gu(2)
                                    rightMargin: units.gu(2)
                                    verticalCenter: parent.verticalCenter
                              }
                        }

                        MouseArea {
                              id: selectableMouseAreaPasteFromClipboard
                              anchors.fill: parent

                              onClicked: {
                                    PopupUtils.open(confirmPasteFromClipboard)
                                    pageStack.pop()
                              }
                        }

                        Label {
                              text: i18n.tr("Paste from clipboard")
                              anchors {
                                    left: pasteFromClipboardIcon.right
                                    leftMargin: units.gu(2)
                                    rightMargin: units.gu(2)
                                    verticalCenter: parent.verticalCenter
                              }
                        }
                  }

                  //---------------------- COPY TO CLIPBOARD ----------------
                  ListItem {
                        id:copyToClipboardContainer
                        anchors.horizontalCenter: parent.Center
                        height: units.gu(4.5)
                        divider.visible : true

                        Icon {
                              id: copyToClipboardIcon
                              name: "edit-copy"
                              height: units.gu(4)
                              width: units.gu(4)

                              anchors {
                                    leftMargin: units.gu(2)
                                    rightMargin: units.gu(2)
                                    verticalCenter: parent.verticalCenter
                              }
                        }

                        MouseArea {
                              id: selectableMouseAreaCopyToClipboard
                              width: parent.width
                              height: parent.height

                              onClicked: {
                                    textArea.copy();
                                    pageStack.pop()
                              }
                        }

                        Label {
                              text: i18n.tr("Copy to clipboard")
                              anchors {
                                    left: copyToClipboardIcon.right
                                    leftMargin: units.gu(2)
                                    rightMargin: units.gu(2)
                                    verticalCenter: parent.verticalCenter
                              }
                        }
                  }

                  //--------------------- Import site as Text---------------
                  ListItem {
                        id: importContainer
                        anchors.horizontalCenter : parent.Center
                        height: units.gu(4.5)
                        divider.visible : true

                        Icon {
                              id: importIcon
                              name: "import"
                              height: units.gu(4)
                              width: units.gu(4)

                              anchors {
                                    leftMargin: units.gu(2)
                                    rightMargin: units.gu(2)
                                    verticalCenter: parent.verticalCenter
                              }
                        }

                        MouseArea {
                              id: selectableMouseAreaImport
                              width: parent.width
                              height: parent.height

                              onClicked: {
                                    PopupUtils.open(webSiteSelector);
                                    pageStack.pop()
                              }
                        }

                        Label {
                              text: i18n.tr("Import a website as text")
                              anchors {
                                    left: importIcon.right
                                    leftMargin: units.gu(2)
                                    rightMargin: units.gu(2)
                                    verticalCenter: parent.verticalCenter
                              }
                        }
                  }

                  //---------------------- DIGEST --------------
                  ListItem {
                        id:digestContainer
                        anchors.horizontalCenter: parent.Center
                        height: units.gu(4.5)
                        divider.visible : true

                        Label {
                              id: hashIconLabel
                              anchors {
                                    left: parent.left
                                    leftMargin: units.gu(1)
                                    verticalCenter: parent.verticalCenter
                              }
                              text: "#"
                              fontSize: "large"
                              color: theme.palette.normal.backgroundTertiaryText //Ash
                        }

                        MouseArea {
                              id: selectableMouseAreaDigest
                              width: parent.width
                              height: parent.height

                              onClicked: {
                                    PopupUtils.open(digestCalculatorChooser)
                                    pageStack.pop()
                              }
                        }

                        Label {
                              text: i18n.tr("Create hash value of note")
                              anchors {
                                    left: parent.left
                                    leftMargin: units.gu(6)
                                    rightMargin: units.gu(2)
                                    verticalCenter: parent.verticalCenter
                              }
                        }
                  }

                  //---------------------------- Base64 ----------------------------
                  ListItem {
                        id:base64Container
                        anchors.horizontalCenter: parent.Center
                        height: units.gu(4.5)
                        divider.visible : true

                        Label {
                              id: base64IconLabel
                              anchors {
                                    left: parent.left
                                    leftMargin: units.gu(0.5)
                                    verticalCenter: parent.verticalCenter
                              }
                              text: "64"
                              fontSize: "large"
                              color: theme.palette.normal.backgroundTertiaryText //Ash
                        }

                        MouseArea {
                              id: selectableMouseAreaBase64
                              width: parent.width
                              height: parent.height

                              onClicked: {
                                    pageStack.push(base64conversionPage);
                              }
                        }

                        Label {
                              text: i18n.tr("Base 64 encode/decode")
                              anchors {
                                    left: parent.left
                                    leftMargin: units.gu(6)
                                    rightMargin: units.gu(2)
                                    verticalCenter: parent.verticalCenter
                              }
                        }
                  }

                  //--------------QR Code generator ----------------
                  ListItem {
                        id:qrCodeGeneratorContainer
                        anchors.horizontalCenter: parent.Center
                        height: units.gu(4.5)
                        divider.visible : false

                        Icon {
                              id: qrCodeIcon
                              name: "grip-large"
                              height: units.gu(4)
                              width: units.gu(4)

                              anchors {
                                    leftMargin: units.gu(2)
                                    rightMargin: units.gu(2)
                                    verticalCenter: parent.verticalCenter
                              }
                        }

                        MouseArea {
                              id: selectableQrCodeGenerator
                              width: parent.width
                              height: parent.height

                              onClicked: {
                                    PopupUtils.open(qrCodeWebSiteSelector);
                                    pageStack.pop();

                              }
                              }

                              Label {
                                    text: i18n.tr("Create QR Code")
                                    anchors {
                                          left: qrCodeIcon.right
                                          leftMargin: units.gu(2)
                                          rightMargin: units.gu(2)
                                          verticalCenter: parent.verticalCenter
                                    }
                              }
                  }
                  //------------------------------------------------
            }

      } //Flickable

      Scrollbar {
            height: units.gu(55)
            flickableItem: settingsPageFlickable2
            align: Qt.AlignTrailing
      }
}
