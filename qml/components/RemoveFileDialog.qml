/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This app is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3

/*
   Ask user confirmation before remove from the filesystem the selected file
 */
Dialog {
    id: confirmDeleteFileDialog

    /* the selected file index position in the ListModel */
    property string imageListModelIndex;

    title: i18n.tr("Confirmation")
    text: i18n.tr("Remove")+" "+localFileslistModel.get(imageListModelIndex).file +" "+i18n.tr("file")+ "?"

    Row{
        anchors.horizontalCenter: confirmDeleteFileDialog.Center
        spacing: units.gu(2)

        Button {
            id: closeButton
            text: i18n.tr("Close")
            width: units.gu(12)
            onClicked: PopupUtils.close(confirmDeleteFileDialog)
        }

        Button {
            id: removeButton
            width: units.gu(12)
            text: i18n.tr("Remove")
            color: theme.palette.normal.negative
            onClicked: {
                var fileToDelete = localFileslistModel.get(imageListModelIndex).file; /* without path */
                localFileslistModel.remove(imageListModelIndex);

                if (mainPage.title === fileToDelete) { /* the file to delete is the one currently saved */
                    mainPage.saved = false
                    textArea.text = ""
                    mainPage.openedFileName = "";
                    mainPage.currentFileLabelVisible = false
                }

                fileIO.remove(root.fileSavingPath + fileToDelete)
                deleteOperationResult.text = i18n.tr("File successfully removed")
                removeButton.enabled = false
                removeButton.visible = false
                }
            }
        }

        Row{
            anchors.horizontalCenter: parent.horizontalCenter
            Label{
                text: " "
                id: deleteOperationResult
                font.bold: true
            }
        }
}
