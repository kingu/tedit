/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This app is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3

/*
  Popup shown when the user choose "Save" or "Save as" menu entry
*/
Dialog {
    id: dialogue
    title: i18n.tr("Save as")
    text: i18n.tr("Enter a name for the file to save.")

    Component.onCompleted: fileName.forceActiveFocus()

    TextField {
        id: fileName
        text: "" //root.openedFileName
        inputMethodHints: Qt.ImhNoPredictiveText | Qt.ImhNoAutoUppercase
    }

    Row {
        anchors.horizontalCenter: parent.horizontalCenter
        spacing: units.gu(0.5)

        Label {
            text: i18n.tr("Save as encrypted file")
        }
        CheckBox {
            id: enableEncryptionCheckBox
            checked: false
            onCheckedChanged: {
                /* to have mutual exclusion */
                if(checked){
                    encryptionPasswordText.enabled = true
                }else{
                    encryptionPasswordText.enabled = false
                }
            }
        }
    }

    Row {
        TextField {
            id:encryptionPasswordText
            width: saveButton.width
            enabled: false
            placeholderText: i18n.tr("Encryption password")
            inputMethodHints: Qt.ImhNoPredictiveText | Qt.ImhNoAutoUppercase
        }
    }

    Button {
        id: saveButton
        text: i18n.tr("Save")
        color: theme.palette.normal.positive

        onClicked: {
            PopupUtils.close(dialogue)
            /* eg: newFileName: file:///tmp/.local/share/tedit.danfro/due
                console.log("fileIO.getHomePath():"+fileIO.getHomePath());
            */

            var newFileName;
            var contentToSave;

            if( enableEncryptionCheckBox.checked) {  /* save as encrypted */
                //console.log("Saving As encrypetd with pw: "+encryptionPasswordText.text)
                contentToSave = encrypt(textArea.text, encryptionPasswordText.text);
                /* append XXX suffix to file name */
                newFileName = "file://" + root.fileSavingPath+fileName.text+"-XXX"; 
                mainPage.fileEncrypted = true;
                mainPage.encryptionPassword = encryptionPasswordText.text;

            } else {  /* save as plain text */
                //console.log("Saving As NOT encrypted")
                contentToSave = textArea.text;
                newFileName = "file://" + root.fileSavingPath+fileName.text;
                mainPage.encryptionPassword = "";
                mainPage.fileEncrypted = false;
            }

            if (!fileIO.write(newFileName, contentToSave)) {
                showInfo(i18n.tr("Couldn't write"));
            } else {
                showInfo(i18n.tr("Saved")); /* show an alert poup */
                mainPage.saved = true;
                mainPage.openedFileName = fileIO.getFullName(newFileName)
                mainPage.title = mainPage.openedFileName //fileIO.getFullName(newFileName) /* file name without path */
                mainPage.currentFileLabelVisible = true
            }
        }
    }

    Button {
        text: i18n.tr("Cancel")
        onClicked: PopupUtils.close(dialogue)
    }
 }
