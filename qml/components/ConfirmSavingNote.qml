/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This app is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3

/*
   Ask the user if want save or not the current note before create a new one containing the provided
   Base64 encoded content
*/
Dialog {
    id: confirmSaveDialogue
    title: i18n.tr("Confirmation")
    text: i18n.tr("Existing note was modified, save it?")

    /* Base64 encoded text to insert in the new created note */
    property string base64TextToInsert;

    Row{
        spacing: units.gu(2)

        /* user DON'T want save old note: create a new one overwriting unsaved content */
        Button {
            text: i18n.tr("No")
            width: units.gu(14)
            onClicked: {
                /* reset flags */
                mainPage.saved = false;
                currentFileOpenedLabel.color = '#A40000'
                mainPage.openedFileName = ""
                mainPage.currentFileLabelVisible = false
                textArea.text = base64TextToInsert

                PopupUtils.close(confirmSaveDialogue)

                pageStack.push(mainPage)
            }
        }

        /* user want save note content and after create a new one */
        Button {
            text: i18n.tr("Yes")
            color: theme.palette.normal.positive
            width: units.gu(14)
            onClicked: {
                if (mainPage.openedFileName == "") {
                    //console.log("Note content was never saved (is new) show saving form");
                    saveRow.visible = true
                } else {
                    /* Note already created: just save it */
                    saveExistingFile(mainPage.openedFileName,root.fileSavingPath)

                    /* reset flags and set new note content at Base64 result */
                    mainPage.saved = true;
                    currentFileOpenedLabel.color = '#A40000'
                    mainPage.openedFileName = ""
                    mainPage.currentFileLabelVisible = false
                    
                    textArea.text = base64TextToInsert
                    PopupUtils.close(confirmSaveDialogue)
                    pageStack.pop(base64ConversionPage)
                }
            }
        }
    }

    /* shown if the user want save note currently content  */
    Row{
        id:saveRow
        visible: false
        spacing: units.gu(0.5)

        TextField {
            id: fileName
            placeholderText: i18n.tr("file name")
            inputMethodHints: Qt.ImhNoPredictiveText | Qt.ImhNoAutoUppercase
        }

        Button {
            text: i18n.tr("Ok")
            color: theme.palette.normal.positive
            width: units.gu(7)
            onClicked: {

                var newFileName = "file://" + root.fileSavingPath + fileName.text;

                if(!fileIO.write(newFileName, textArea.text)) {
                    showInfo(i18n.tr("Couldn't write"));
                } else {
                    showInfo(i18n.tr("Saved"));

                    /* reset flags and set new note content at Base64 result */
                    mainPage.saved = true;
                    currentFileOpenedLabel.color = '#A40000'
                    mainPage.openedFileName = ""
                    mainPage.currentFileLabelVisible = false
                    textArea.text = base64TextToInsert

                    PopupUtils.close(confirmSaveDialogue)

                    pageStack.pop(base64ConversionPage)
                    //pageStack.push(mainPage)
                }
            }
        }
    }
}
