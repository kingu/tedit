# tedit

A useful notes text editor for Ubuntu Touch. Write down things in simple text format.

Some additional features are:

- calculating hash values
- encrypting of notes
- creating of QR codes
- importing of websites as simple text

This app is **soon** available in the OpenStore:

[![OpenStore](https://open-store.io/badges/en_US.png)](https://open-store.io/app/tedit.danfro)

## Credits

Originally based on the App "edit" by Paweł Stroka (not released to OpenStore) and brought to OpenStore by fulvio.
This is the second fork of fulvio's app version to keep the app maintained and available for users.

## File storage location

In case you wish to sync your files with another app, tedit files are stored in the apps `~/.local/share` folder. This should be `/home/phablet/.local/share/tedit.danfro`.

## Translating

Tedit app can be translated on [Hosted Weblate](https://hosted.weblate.org/projects/ubports/tedit/). The localization platform of this project is sponsored by Hosted Weblate via their free hosting plan for Libre and Open Source Projects.

We welcome translators from all different languages. Thank you for your contribution!
You can easily contribute to the localization of this project (i.e. the translation into your language) by visiting (and signing up with) the Hosted Weblate service as linked above and start translating by using the webinterface. To add a new language, log into weblate, goto tools --> start new translation.